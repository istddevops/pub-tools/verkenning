# Uitleg

## Informatiemodel Wlz

| Informatiemodel Wlz |
|-|
| Dit is het Informatiemodel voor de Wlz (Wet langdurige zorg) van Zorginstituut Nederland.U vindt hier de specificaties van de iWlz standaard release 2.2. Het Informatiemodel brengt de samenhang in processen, bedrijfsregels en berichtspecificaties in kaart. | 
| Deze elementen zijn onlosmakelijk met elkaar verbonden: ze versterken óf beperken elkaar. Het Informatiemodel Wet langdurige zorg geeft een integraal overzicht van het Wlz-berichtenverkeer zoals het er vanaf 1 januari 2021 uitziet. |
| iWlz is onderdeel van de familie van iStandaarden die beheerd wordt door Zorginstituut Nederland. Door een integrale aanpak van de doorontwikkeling van de iStandaarden bewaakt het Zorginstituut de onderlinge consistentie van de standaarden |

