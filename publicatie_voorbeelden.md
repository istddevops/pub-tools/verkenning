# Publicatie voorbeelden

- [Groepssite publicatie verkenning in VuePress-MdBook](https://istddev.gitlab.io)
- [Formele standaardspecificatie in W3C-ResPec](https://istddev.gitlab.io/ecosysteem/bronhouder/gevalideerde-vraag)
- [Generatie van publicatie uit modellen](https://istddev.gitlab.io/kik-v/sparql2doc/)
- [Generatie van een publicatie uit een document](https://istddev.gitlab.io/kik-v/explainer/)

*Nog andere ideen?*
