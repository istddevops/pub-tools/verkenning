# Contributing

## Designers / Developers

- ![1](https://assets.gitlab-static.net/uploads/-/system/user/avatar/8167154/avatar.png?width=40) [Onno Haldar](https://gitlab.com/onnohaldar)

- [Fabien Reiniers](https://gitlab.com/FabienReniers)

- [Elly Kampert](https://gitlab.com/EKampert)

- [Roberto Lambooy](https://gitlab.com/roberto.lambooy)

## Reviewers / Testers

- pm

[![Ecosysteem: NPM](https://img.shields.io/badge/NPM-Ecosystem-brightgreen)](https://www.npmjs.com/)
